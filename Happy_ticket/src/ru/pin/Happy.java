package ru.pin;


public class Happy {
   public static String ticket(String str) {

       try {
           System.out.println(str);
       } catch (NumberFormatException e) {
           System.err.println("Неверный формат строки!");
       }
       //Разделение строки на подстроки
       String[] arr_tickets = str.split("\n");

       //Массивы для хранения цифр счастливого билета
       int[] number = new int[6];
       String[] number_str;

       String happy = "";

       for (int j = 0; j < arr_tickets.length; j++){
           number_str = arr_tickets[j].split("");

           for (int i = 0; i < number_str.length; i++){
               number[i] = Integer.parseInt(number_str[i]);
           }

            if(number[0]+number[1]+number[2] == number[3]+number[4]+number[5]){
                happy += arr_tickets[j] + " ";
            }
       }

       return (happy);
   }
}
