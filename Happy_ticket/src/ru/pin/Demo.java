package ru.pin;

import java.io.FileNotFoundException;


public class Demo {
    private static String fileNameWrite = "file.txt";
    private static String fileNameRead = "result.txt";

    public static void main(String args[]) throws FileNotFoundException{

        String strFromFile = FileWorker.read(fileNameWrite);

        FileWorker.write(fileNameRead, Happy.ticket(strFromFile));
    }
}
