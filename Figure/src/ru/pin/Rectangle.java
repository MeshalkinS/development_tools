package ru.pin;


public class Rectangle extends Figure {

    public Rectangle(double sideA, double sideB){
        super(sideA,sideB);
    }

    public Rectangle(double sideA){
        super(sideA);
    }

    public Rectangle(){
        super();
    }

    @Override
    void output() {
        System.out.println("Площадь четырёхугольника: " + squareFigure());
    }

    @Override
    double squareFigure() {
        return sideA*sideB;
    }
}

