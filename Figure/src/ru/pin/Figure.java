package ru.pin;


public abstract class Figure {

    double sideA;
    double sideB;

    abstract void output();
    abstract double squareFigure();

    Figure(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    Figure(double sideA){
        this(sideA, sideA);
    }

    Figure(){
        this(1);
    }
}
