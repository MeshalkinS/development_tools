package ru.pin;


public class Triangle extends Figure {

    public Triangle(double sideA, double sideB){
        super(sideA, sideB);
    }

    public Triangle(double sideA){
        super(sideA);
    }

    public Triangle(){
        super();
    }

    @Override
    void output() {
        System.out.println("Площадь треугольника: " + squareFigure());
    }

    @Override
    double squareFigure() {
        return (sideA*sideB)/2;
    }
}
