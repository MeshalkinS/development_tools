package ru.pin.circle;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class Demo {
    public static void main(String[] args) {
    Circle ring1 = new Circle();
    Circle ring2;

        try{
            ring2 = new Circle(1, 22, -6);
        }catch(IllegalArgumentException e){
            Logger.getLogger(Circle.class.getName()).log(new LogRecord(Level.WARNING,
                    "Был передан аргумент с негативным значением!"));
            throw e;
        }

    ring1.printCircle();
    ring2.printCircle();

    ring1.inputCircle();
    ring1.printCircle();

    System.out.println("Длина окружности ring1 = "+ring1.lengthCircle());
    System.out.println("Площадь круга ring1 = "+ring1.squareCircle());

    if (ring1.equalityCircle(ring2)) {
        System.out.println("Площади равны");
    }
    else{
        System.out.println("Площади не равны");
    }

    System.out.println("Расстояние между центрами окружностей ring1 и ring2 = " + ring1.getDistance(ring2));

    if (ring1.intersectionCircle(ring2)) {
        System.out.println("Окружности имеют одну точку касания");
    }
    else{
        System.out.println("Окружности не имеют или имеют несколько точек касания ");
    }

    System.out.println("Окружность ring1 переместилась");
    ring1.moveCircle();
    ring1.printCircle();
}
}
