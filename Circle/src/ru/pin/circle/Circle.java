package ru.pin.circle;

import java.util.Scanner;

public class Circle {
    double x;
    double y;
    double r;

    public Circle(double newX, double newY, double newR){
        x = newX;
        y = newY;
        r = newR;
        if(newR <= 0)
            throw new IllegalArgumentException("value of 'newR' is negative: r=" + newR);

    }

    public Circle(){
        x = 0;
        y = 0;
        r = 1.0;
    }

    void setCircle(double newX, double newY, double newR) {
        x = newX;
        y = newY;
        r = newR;
    }

    void inputCircle(){
        Scanner inp = new Scanner(System.in);
        double newX = 0, newY = 0, newR = 0;


                System.out.print("Введите абсциссу: ");
                newX = inp.nextDouble();
                System.out.print("Введите ординату: ");
                newY = inp.nextDouble();
                System.out.print("Введите радиус: ");
                newR = inp.nextDouble();

                inp.close();
                setCircle(newX, newY, newR);

    }

    void printCircle(){
        System.out.println("Центр окружности ("+x+";"+y+")  радиус "+r);
    }

    public double lengthCircle(){
        return 2 * Math.PI * r;
    }

    public double squareCircle(){
        return Math.PI * r * r;
    }

    boolean equalityCircle(Circle a){
        if(squareCircle() == a.squareCircle()){
            return true;
        } else {
            return false;
        }
    }

    void moveCircle(){
        x = -99 + (Math.random() * (99 - (-99)));
        y = -99 + (Math.random() * (99 - (-99)));
    }

    double getDistance(Circle a){
        return Math.sqrt(Math.pow(x-a.x, 2) + Math.pow(y-a.y, 2));
    }

    boolean intersectionCircle(Circle a){

        if(this.getDistance(a) == (a.r + r)){
            return true;
        }
        if(this.getDistance(a) == (r - a.r) ||
                this.getDistance(a) == (a.r - r)){
            return true;
        }
        else{
            return false;
        }
    }
}
