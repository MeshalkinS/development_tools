package ru.pin;
/**
 * Родительский класс для организации классификации транспортных средст
 *
 * @author Мешалкин С. 12ОИТ18К
 */
public class Vehicle {
    /**
     * @param type 1 - Earth, 2 - Water, 3 - Sky.
     * @param maxSpeed Максимальная скорость транспорта.
     */
    private int type;
    private int maxSpeed;

    public Vehicle(int type, int maxSpeed) {
        this.type = type;
        this.maxSpeed = maxSpeed;
    }

    public void Message(){
        System.out.println("This is a message from Vehicle class");
    }

    public int getType() {
        return this.type;
    }

    public String getTypeAsString(){
        switch (this.type) {
            case 1:
                return "Earth";
            case 2:
                return "Water";
            case 3:
                return "Sky";
        }
        return "";
    }

    public int getMaxSpeed() {
        return this.maxSpeed;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String toString() {
        return "type: " + this.getTypeAsString() + ", max speed: " + this.maxSpeed;
    }
}

