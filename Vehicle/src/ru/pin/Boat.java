package ru.pin;

/**
 * Класс для описания водного транспорта
 *
 * @author Мешалкин С. 12ОИТ18К
 */
public class Boat extends Vehicle {
    public Boat(int maxSpeed) {
        super(2, maxSpeed);
    }

    public void Message() {
        super.Message();
        System.out.println("This is a message from Boat class");
    }

    public String toString() {
        return "Boat: " + super.toString();
    }

}
