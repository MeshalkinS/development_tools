package ru.pin;

/**
 * Класс для описания воздушного транспорта
 *
 * @author Мешалкин С. 12ОИТ18К
 */
class Airplane extends Vehicle {
    /**
     * @param maxHeight Максимальная высота полёт.
     */
    private int maxHeight;

    public Airplane(int maxSpeed, int maxHeight) {
        super(3, maxSpeed);
        this.maxHeight = maxHeight;
    }

    public void Message(){
        super.Message();
        System.out.println("This is a message from Airplane class");
    }

    public String toString() {
        return "Airplane: " + super.toString() + ", max height: " + this.maxHeight;
    }

}
