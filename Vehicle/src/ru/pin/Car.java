package ru.pin;

/**
 * Класс для описания легкового автомобиля
 *
 * @author Мешалкин С. 12ОИТ18К
 */
public class Car extends Vehicle {
    /**
     * @param wheels Количество колёс в транспортном средстве.
     */
    private int wheels;

    public Car(int maxSpeed, int wheels) {
        super(1, maxSpeed);
        this.wheels = wheels;
    }

    public void Message() {
        super.Message();
        System.out.println("This is a message from Car class");
    }

    public String toString() {
        return "Car: " + super.toString() + ", wheels: " + this.wheels;
    }

}
