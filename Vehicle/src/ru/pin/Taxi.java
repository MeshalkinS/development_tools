package ru.pin;

/**
 * Класс для описания пассажирского транспорта
 *
 * @author Мешалкин С. 12ОИТ18К
 */
public class Taxi extends Car {
    /**
     * @param passengers Количество возможных пассажиров.
     */
    private int passengers;

    public Taxi(int passengers) {
        super(200, 4);
        this.passengers = passengers;
    }

    public void Message() {
        super.Message();
        System.out.println("This is a message from Taxi class");
    }

    public String toString() {
        return "Taxi: " + super.toString() + ", passengers: " + this.passengers;
    }

}
