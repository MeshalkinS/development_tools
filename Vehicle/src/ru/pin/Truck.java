package ru.pin;

/**
 * Класс для описания грузового автомобиля
 *
 * @author Мешалкин С. 12ОИТ18К
 */
public class Truck extends Car{
    /**
     * @param maxMass Максимальная масса перевозимого груза.
     */
    private int maxMass;

    public Truck(int maxMass){
        super(220, 4);
        this.maxMass = maxMass;
    }

    public void Message() {
        super.Message();
        System.out.println("This is a message from Truck class");
    }

    public String toString() {
        return "Truck: " + super.toString() + ", MaxMass: " + this.maxMass;
    }

}
